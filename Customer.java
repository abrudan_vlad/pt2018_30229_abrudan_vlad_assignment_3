package Model;

import DataAccess.GenericAccess;

public class Customer extends GenericAccess<Customer> {
	private int id;
	private String name;
	private int age;
	private String email;
    private String adress;
    
    public Customer(int id,String name,int age,String email,String adress) {
    	this.adress=adress;
    	this.age=age;
    	this.email=email;
    	this.id=id;
    	this.name=name;
    } 
    
    public Customer() {
    	this.id=0;
    	this.name="";
    	this.age=0;
    	this.email="";
    	this.adress="";
    }
    
    public Customer(int id) {
    	this.id=id;
    }
    
    /**
     * metoda are posibilitatea de a prelua adresa clientului
     * @return returneaza adresa
     */
    public String getAdress() {
		return adress;
	}
    /**
     * metoda are posibilitatea de a prelua varsta clientului
     * @return returneaza varsta
     */
    public int getAge() {
		return age;
	}
    /**
     * metoda are posibilitatea de a prelua emailul clientului
     * @return returneaza emailul
     */
    public String getEmail() {
		return email;
	}
    
    /**
     * metoda are posibilitatea de a prelua id-ul unic al  clientului
     * @return returneaza id-ul
     */
    public int getId() {
		return id;
	}
    /**
     * metoda are posibilitatea de a prelua numele clientului
     * @return returneaza numele
     */
    public String getName() {
		return name;
	}
    
    /**
     * metoda atribuie atributului "id" o valoare
     * @param id id-ul clientului
     */
    public void setId(int id) {
		this.id = id;
	}
    
    /**
     * metoda tribuie atributului "adress" o valoare
     * @param adress adresa clientului
     */
    public void setAdress(String adress) {
		this.adress = adress;
	}
    /**
     * metoda tribuie atributului "age" o valoare
     * @param age varsta clientului
     */
    public void setAge(int age) {
		this.age = age;
	}
    /**
     * metoda tribuie atributului "email" o valoare
     * @param email email-ul clientului
     */
    public void setEmail(String email) {
		this.email = email;
	}
    /**
     * metoda tribuie atributului "name" o valoare
     * @param name numele clientului
     */
    public void setName(String name) {
		this.name = name;
	}
    
 
}
