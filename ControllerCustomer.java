package BussinesLogic;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Formatter;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import DataAccess.GenericAccess;
import Model.Customer;
import Model.Orders;
import Model.Product;
import View.TableView;
import View.ViewCustomer;

public class ControllerCustomer {
private ViewCustomer viewCustomer=new ViewCustomer();

public ControllerCustomer() {
	Product product=new Product();
	Customer customer=new Customer();
	viewCustomer.getSeeAllOreders().addActionListener(new ListennerSeeAllOrders());
	populateTable(viewCustomer.getCustomersTable(),customer.tableHeader(customer.createTableHeader()),customer.findAllData(customer.execQuery(customer.createFindAllQuery())));
	populateTable(viewCustomer.getProductsTable(),product.tableHeader(product.createTableHeader()),product.findAllData(product.execQuery(product.createFindAllQuery())));
	viewCustomer.getComand().addActionListener(new ListennerComand());

}

/**
 * metoda populeaza un tabel cu date si ii seteaza headerele
 * @param table tabelul ce trebuie populat cu date
 * @param headers lista de stringuri ce vor servi drept headere tabelului
 * @param data datele ce vor fi introduse in tabel
 * @return tabelul populat cu date
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public JTable populateTable(JTable table,ArrayList<String> headers, ArrayList<?> data) {
	DefaultTableModel defaultTableModel=new DefaultTableModel(new Object[][] {},headers.toArray());
	table.setModel(defaultTableModel);
	ArrayList<String> strings=new ArrayList<>();
	for (Object object : data) {
		strings=((GenericAccess)object).getValues(((GenericAccess)object).createTableHeader());
		defaultTableModel.addRow(strings.toArray());
	}
	table.setBackground(Color.cyan);
	return table;
}

class ListennerComand implements ActionListener{
	Product product1;
	public void actionPerformed(ActionEvent e) {
		try {
			int idCust=Integer.parseInt(viewCustomer.getIdCustomer().getText());
			int idProd=Integer.parseInt(viewCustomer.getIdProduct().getText());
			int id=Integer.parseInt(viewCustomer.getId().getText());
			int quantity=Integer.parseInt(viewCustomer.getQuantity().getText());
			String productName=viewCustomer.getNameProduct().getText();
			String customerName=viewCustomer.getNameCustomer().getText();
			String date=viewCustomer.getDate().getText();
			if(productName!="" && customerName!="" && date!="" && quantity>0) {
				product1=new Product(idProd);
				int quantityToUpdate=(product1.findAllData(product1.execQuery(product1.createFindById(product1.getId())))).get(0).getQuantity();
				int idToUpdate=(product1.findAllData(product1.execQuery(product1.createFindById(product1.getId())))).get(0).getId();
				String nameToUpdate=(product1.findAllData(product1.execQuery(product1.createFindById(product1.getId())))).get(0).getName();
				String descriptionToUpdate=(product1.findAllData(product1.execQuery(product1.createFindById(product1.getId())))).get(0).getDescription();
				int priceToUpdate=(product1.findAllData(product1.execQuery(product1.createFindById(product1.getId())))).get(0).getPrice();
				if(quantityToUpdate>quantity) {
					Orders orders=new Orders(id, idCust, idProd, date, customerName, productName,quantity);
					try {
						orders.insert();
						createBill(orders);
					}
					catch (Exception e1) {
						JOptionPane.showMessageDialog(null, "Date introduse incorecte!");	
					}
					Product productToUpdate=new Product(idToUpdate, nameToUpdate, quantityToUpdate-quantity, priceToUpdate, descriptionToUpdate);
					try {
						productToUpdate.update();
					}
					catch (Exception e3) {
						JOptionPane.showMessageDialog(null, "Date introduse incorecte!");	
					}
					populateTable(viewCustomer.getProductsTable(),product1.tableHeader(product1.createTableHeader()),product1.findAllData(product1.execQuery(product1.createFindAllQuery())));

				}
				else
					JOptionPane.showMessageDialog(null, "Date introduse incorecte!");	
			}
			else
				JOptionPane.showMessageDialog(null, "Date introduse incorecte!");
			} catch (Exception e2) {
			JOptionPane.showMessageDialog(null, "Date introduse incorecte!");
		}
	}	
}

class ListennerSeeAllOrders implements ActionListener{
	Orders order=new Orders();
	public void actionPerformed(ActionEvent e) {
		@SuppressWarnings("unused")
		TableView tableView=new TableView(populateTable(new JTable(), order.tableHeader(order.createTableHeader()), order.findAllData(order.execQuery(order.createFindAllQuery()))));	
	}
}

/**
 * metoda deschide un fisier, scrie in interiorul lui detaliile unei comenzi si il inchide
 * @param orders obiect ale carui atribute definesc o facutra
 */
public void createBill(Orders orders) {
	Formatter x=null;
	try {
		x=new Formatter("Bill"+orders.getId()+".txt");
		x.format("Fatura cu numarul %s.\n\nCumparator:%s cu id-ul:%s\n\n Produs achizitionat:%s cu id-ul:%s\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n Va multumim!!!!\n",orders.getId(),orders.getCustomerName(),orders.getCustomerId(),orders.getProductName(),orders.getProductId());
		x.close();
	} catch (Exception e) {
		e.printStackTrace();
	}
}
  
}
