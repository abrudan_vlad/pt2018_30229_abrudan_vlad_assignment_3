package View;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ViewCustomer extends JFrame {
	private JButton comand=new JButton("Make an order");
	private JButton seeAllOreders=new JButton("See all orders");
	private JTextArea id=new JTextArea();
	private JTextArea idCustomer=new JTextArea();
	private JTextArea idProduct=new JTextArea();
	private JTextArea nameCustomer=new JTextArea();
	private JTextArea nameProduct=new JTextArea();
	private JTextArea date=new JTextArea();
	private JTextArea quantity=new JTextArea();
	private JTable customersTable=new JTable();
	private JTable productsTable=new JTable();
	
	public ViewCustomer() {
		JScrollPane scrollPane1=new JScrollPane(customersTable);
		JScrollPane scrollPane2=new JScrollPane(productsTable);
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JPanel p4=new JPanel();
		p4.setLayout(new BoxLayout(p4,BoxLayout.Y_AXIS));
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
		p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
		p2.add(createPane(id, new JLabel("Order id")));
		p2.add(createPane(idCustomer, new JLabel("Customer Id")));
		p2.add(createPane(idProduct, new JLabel("Product Id")));
		p2.add(createPane(nameCustomer,new JLabel("Customer name")));
		p2.add(createPane(nameProduct,new JLabel("Product name")));
		p2.add(createPane(date, new JLabel("Date")));
		p2.add(createPane(quantity, new JLabel("Product quantity")));
		p3.add(dimensionare(comand));
		p3.add(dimensionare(seeAllOreders));
		p4.add(dimensionare(scrollPane1));
		p4.add(dimensionare(scrollPane2));
		p1.add(p2);
		p1.add(p3);
		p1.add(p4);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Customer's Pane");
		this.setSize(new Dimension(900,700));
		this.setLocation(400, 10);
		this.setContentPane(p1);
		this.setVisible(true);
		
	}
	/**
	 * Creaza un panel nou cu un Layout de tip box orientat pe axa y
	 * @param textArea textArea adaugat in panel
	 * @param label label adaugat in panel
	 * @return panel cu Layout de tip box
	 */
	public JPanel createPane(JTextArea textArea,JLabel label) {
		JPanel panel=new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(label);
		textArea.setMaximumSize(new Dimension(200,20));
		textArea.setMinimumSize(new Dimension(100, 20));
		panel.add(textArea);
		return panel;
	}
	/**
	 * @param button buton ce urmeaza a fi dimensionat
	 * @return returneaza un buton redimensionat
	 */
	public JButton dimensionare(JButton button) {
		button.setMaximumSize(new Dimension(150, 30));
		return button;
	}
	

	/**
	 * @param scrollPane ce urmeaza a fi dimensionat
	 * @return returneaza un JScrollPane redimensionat
	 */
	public JScrollPane dimensionare(JScrollPane scrollPane) {
		scrollPane.setMaximumSize(new Dimension(500, 300));
		return scrollPane;
	}

    public JButton getComand() {
		return comand;
	}
    
    public JTextArea getDate() {
		return date;
	}
    
    public JTextArea getId() {
		return id;
	}
    
    public JTextArea getIdCustomer() {
		return idCustomer;
	}
    
    public JTextArea getIdProduct() {
		return idProduct;
	}
    
    public JTextArea getNameCustomer() {
		return nameCustomer;
	}
    
    public JTextArea getNameProduct() {
		return nameProduct;
	}
    
    public JTextArea getQuantity() {
		return quantity;
	}
    
    public JButton getSeeAllOreders() {
		return seeAllOreders;
	}
    
    public JTable getCustomersTable() {
		return customersTable;
	}
    
    public JTable getProductsTable() {
		return productsTable;
	}
	
}
