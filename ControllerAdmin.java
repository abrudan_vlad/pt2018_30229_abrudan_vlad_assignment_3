package BussinesLogic;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import DataAccess.GenericAccess;
import Model.Customer;
import Model.Product;
import Model.Provider;
import View.TableView;
import View.ViewAdmin;

public class ControllerAdmin {
	private ViewAdmin viewAdmin=new ViewAdmin();
	
    public ControllerAdmin() {
	  viewAdmin.getSeeAllProducts().addActionListener(new ListennerSeeAllProducts());
	  viewAdmin.getAddProduct().addActionListener(new ListennerAddProduct());
	  viewAdmin.getDeleteProduct().addActionListener(new ListennerDeleteProduct());
	  viewAdmin.getUpdateProduct().addActionListener(new ListennerUpdateProduct());
	  viewAdmin.getSeeAllCustomers().addActionListener(new ListnnerSeeAllCustomers());
	  viewAdmin.getAddCustomer().addActionListener(new ListennerAddCustomer());
	  viewAdmin.getDeleteCustomer().addActionListener(new ListennerDeleteCustomer());
	  viewAdmin.getUpdateCustomer().addActionListener(new ListennerUpdateCustomer());  
  } 

    /**
     * metoda populeaza un tabel cu date si ii seteaza headerele
     * @param table tabelul ce trebuie populat cu date
     * @param headers lista de stringuri ce vor servi drept headere tabelului
     * @param data datele ce vor fi introduse in tabel
     * @return tabelul populat cu date
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public JTable populateTable(JTable table,ArrayList<String> headers, ArrayList<?> data) {
	    	DefaultTableModel defaultTableModel=new DefaultTableModel(new Object[][] {},headers.toArray());
	    	table=new JTable(defaultTableModel);
	    	table.setModel(defaultTableModel);
	    	ArrayList<String> strings=new ArrayList<>();
	    	for (Object object : data) {
				strings=((GenericAccess)object).getValues(((GenericAccess)object).createTableHeader());
				defaultTableModel.addRow(strings.toArray());
			}
	    	table.setBackground(Color.cyan);
	    	return table;
	    }
	 
	class ListennerSeeAllProducts implements ActionListener{
		private Product product=new Product();
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			TableView tableView=new TableView(populateTable(new JTable(), product.tableHeader(product.createTableHeader()), product.findAllData(product.execQuery(product.createFindAllQuery()))));
		}
		
	}
	
	
	class ListennerAddProduct implements ActionListener{
		private Product product;
		public void actionPerformed(ActionEvent e) {
			try {
				Provider provider=new Provider(Integer.parseInt(viewAdmin.getIdProduct().getText()));
				int quantity=provider.findAllData(provider.execQuery(provider.createFindById(provider.getId()))).get(0).getProductQuantity();
				if(Integer.parseInt(viewAdmin.getQuantityProduct().getText())<quantity && Integer.parseInt(viewAdmin.getQuantityProduct().getText())>0) {
					product=new Product(Integer.parseInt(viewAdmin.getIdProduct().getText()), viewAdmin.getNameProduct().getText(),Integer.parseInt(viewAdmin.getQuantityProduct().getText()), Integer.parseInt(viewAdmin.getPriceProduct().getText()), viewAdmin.getDescriptionProduct().getText());
					product.insert();
					}
				else {
					JOptionPane.showMessageDialog(null,"Date sau cantitati introduse incorect!");
					}
				}catch (Exception e1) {
					JOptionPane.showMessageDialog(null,"Date sau cantitati introduse incorect!");
				}
		}
		
	}
	
	class ListennerDeleteProduct implements ActionListener{
		private Product product;
		public void actionPerformed(ActionEvent e) {
			product=new Product(Integer.parseInt(viewAdmin.getIdProduct().getText()));
			product.delete();
		}
		
	}
	
	class ListennerUpdateProduct implements ActionListener{
		private Product product;

		public void actionPerformed(ActionEvent e) {
			product=new Product(Integer.parseInt(viewAdmin.getIdProduct().getText()), viewAdmin.getNameProduct().getText(),Integer.parseInt(viewAdmin.getQuantityProduct().getText()), Integer.parseInt(viewAdmin.getPriceProduct().getText()), viewAdmin.getDescriptionProduct().getText());
			product.update();
		}
	}
	
	class ListnnerSeeAllCustomers implements ActionListener{
		Customer customer=new Customer();

		public void actionPerformed(ActionEvent e) {
			//customer=new Customer(Integer.parseInt(viewAdmin.getIdCustomer().getText()), viewAdmin.getNameCustomer().getText(), Integer.parseInt(viewAdmin.getAgeCustomer().getText()), viewAdmin.getEmailCustomer().getText(), viewAdmin.getAdressCustomer().getText());
			@SuppressWarnings("unused")
			TableView tableView=new TableView(populateTable(new JTable(), customer.tableHeader(customer.createTableHeader()), customer.findAllData(customer.execQuery(customer.createFindAllQuery()))));
		}
		
	}
	
	class ListennerAddCustomer implements ActionListener{
		private Customer customer;

		
		public void actionPerformed(ActionEvent e) {
			customer=new Customer(Integer.parseInt(viewAdmin.getIdCustomer().getText()), viewAdmin.getNameCustomer().getText(), Integer.parseInt(viewAdmin.getAgeCustomer().getText()), viewAdmin.getEmailCustomer().getText(), viewAdmin.getAdressCustomer().getText());
			customer.insert();
		}
		
	}
	
	class ListennerDeleteCustomer implements ActionListener{
		private Customer customer;


		public void actionPerformed(ActionEvent e) {
			customer=new Customer(Integer.parseInt(viewAdmin.getIdCustomer().getText()));
			customer.delete();
		}
		
	}
	class ListennerUpdateCustomer implements ActionListener{
		private Customer customer;

		public void actionPerformed(ActionEvent e) {
			customer=new Customer(Integer.parseInt(viewAdmin.getIdCustomer().getText()), viewAdmin.getNameCustomer().getText(), Integer.parseInt(viewAdmin.getAgeCustomer().getText()), viewAdmin.getEmailCustomer().getText(), viewAdmin.getAdressCustomer().getText());
			customer.update();
		}
	}

}
