package View;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class ViewAdmin extends JFrame {
	private JButton addProduct=new JButton("Add a new product"); 
	private JButton deleteProduct=new JButton("Delete a product");
	private JButton updateProduct=new JButton("Update a product");
	private JButton seeAllProducts=new JButton("See all products");
	private JTextArea idProduct=new JTextArea();
	private JTextArea nameProduct=new JTextArea();
	private JTextArea quantityProduct=new JTextArea();
	private JTextArea priceProduct=new JTextArea();
	private JTextArea descriptionProduct=new JTextArea();
	private JButton addCustomer=new JButton("Add a new customer");
	private JButton deleteCustomer=new JButton("Delete a customer");
	private JButton updateCustomer=new JButton("Update a customer");
	private JButton seeAllCustomers=new JButton("See all customers");
	private JTextArea idCustomer=new JTextArea();
	private JTextArea nameCustomer=new JTextArea();
	private JTextArea ageCustomer=new JTextArea();
	private JTextArea emailCustomer=new JTextArea();
	private JTextArea adressCustomer=new JTextArea();
	
	public ViewAdmin() {
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JPanel p4=new JPanel();
		JPanel p5=new JPanel();
		JPanel p6=new JPanel();
		p6.setMaximumSize(new Dimension(50, 50));
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
		p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
		p2.add(createPane(idProduct, new JLabel("Product id")));
		p2.add(createPane(nameProduct, new JLabel("Product name")));
		p2.add(createPane(quantityProduct, new JLabel("Product quantity")));
		p2.add(createPane(priceProduct,new JLabel("Product price")));
		p2.add(createPane(descriptionProduct, new JLabel("Product description")));
		p3.add(dimensionare(addProduct));
		p3.add(dimensionare(deleteProduct));
		p3.add(dimensionare(updateProduct));
		p3.add(dimensionare(seeAllProducts));
		p4.setLayout(new BoxLayout(p4, BoxLayout.Y_AXIS));
		p5.setLayout(new BoxLayout(p5, BoxLayout.Y_AXIS));
		p4.add(createPane(idCustomer, new JLabel("Customer id")));
		p4.add(createPane(nameCustomer, new JLabel("Customer name")));
		p4.add(createPane(ageCustomer, new JLabel("Customer age")));
		p4.add(createPane(emailCustomer, new JLabel("Customer email")));
		p4.add(createPane(adressCustomer, new JLabel("Customer adress")));
		p5.add(dimensionare(addCustomer));
		p5.add(dimensionare(deleteCustomer));
		p5.add(dimensionare(updateCustomer));
		p5.add(dimensionare(seeAllCustomers));
		p1.add(p2);
		p1.add(p3);
		p1.add(p6);
		p1.add(p4);
		p1.add(p5);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(1000,500));
		this.setLocation(200, 10);
		this.setContentPane(p1);
		this.setTitle("Admin's Pane");
		this.setVisible(true);
		
	}

	/**
	 * Creaza un panel nou cu un Layout de tip box orientat pe axa y
	 * @param textArea textArea adaugat in panel
	 * @param label label adaugat in panel
	 * @return panel cu Layout de tip box
	 */
	public JPanel createPane(JTextArea textArea,JLabel label) {
		JPanel panel=new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.add(label);
		textArea.setMaximumSize(new Dimension(200,20));
		textArea.setMinimumSize(new Dimension(100, 20));
		panel.add(textArea);
		return panel;
	}
	
	/**
	 * @param button buton ce urmeaza a fi dimensionat
	 * @return returneaza u buton redimensionat
	 */
	public JButton dimensionare(JButton button) {
		button.setMaximumSize(new Dimension(150, 30));
		return button;
	}

	public JButton getAddProduct() {
		return addProduct;
	}
	
	public JButton getSeeAll() {
		return seeAllProducts;
	}
	
	public JButton getUpdateProduct() {
		return updateProduct;
	}
	
	
    public JButton getDeleteProduct() {
			return deleteProduct;
	}
    
    
    public JTextArea getNameText() {
		return nameProduct;
	}
    
    public JTextArea getIdText() {
		return idProduct;
	}
    
    public JButton getUpdateCustomer() {
		return updateCustomer;
	}
    
    public JButton getSeeAllProducts() {
		return seeAllProducts;
	}
    
    public JButton getSeeAllCustomers() {
		return seeAllCustomers;
	}
    
    public JTextArea getQuantityProduct() {
		return quantityProduct;
	}
    
    public JTextArea getPriceProduct() {
		return priceProduct;
	}
    
    public JTextArea getNameProduct() {
		return nameProduct;
	}
    
    public JTextArea getNameCustomer() {
		return nameCustomer;
	}
    
    public JTextArea getIdProduct() {
		return idProduct;
	}
    
    public JTextArea getIdCustomer() {
		return idCustomer;
	}
    
    public JTextArea getEmailCustomer() {
		return emailCustomer;
	}
    
    public JTextArea getDescriptionProduct() {
		return descriptionProduct;
	}
    
    public JButton getDeleteCustomer() {
		return deleteCustomer;
	}
    
    public JTextArea getAgeCustomer() {
		return ageCustomer;
	}
    
    public JTextArea getAdressCustomer() {
		return adressCustomer;
	}
    
    public JButton getAddCustomer() {
		return addCustomer;
	}
	
}
