package Model;

import DataAccess.GenericAccess;

public class Product extends GenericAccess<Product>{
	private int id;
	private String name;
	private int quantity;
	private int price;
	private String description;
	
	public Product() {
		this.id=0;
		this.name="";
		this.quantity=0;
		this.price=0;
	}
	
	public Product(int id, String name, int quantity, int price, String description) {
		this.id=id;
		this.name=name;
		this.price=price;
		this.quantity=quantity;
		this.description=description;
	}
	
	public Product(int id) {
		this.id=id;
	}
	
	/**
	 * @return returneaza valoarea atributului id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return returneaza valoarea atributului price
	 */
	public int getPrice() {
		return price;
	}
	
	/**
	 * @return returneaza valoarea campului name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return returneaza valoarea campului quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * Seteaza valoarea atributului id
	 * @param id id-ul clientului
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Seteaza valoarea atributului name
	 * @param name numele clientului
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Seteaza valoarea atributului price
	 * @param price pretul produsului
	 */
	public void setPrice(int price) {
		this.price = price;
	}
	
	/**
	 * Seteaza valoarea atributului quantity
	 * @param quantity cantitatea produsului
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * @return returneaza valoarea atributului description
	 */
	public String getDescription() {
		return description;
	}
	
	/** Seteaza valoarea atributului description
	 * @param description descrierea produsului
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString() {
		return id+","+name+","+quantity+","+price;
	}
}
