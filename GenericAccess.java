package DataAccess;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenericAccess<T>{
	private Connection connection=ConnectionFactory.getConnection();
	private final Class<T> classType; 
	
	@SuppressWarnings("unchecked")
	public GenericAccess() {
		this.classType=(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
		this.connection=ConnectionFactory.getConnection();
	}
	
	/**
	 * @return returneaza  vectorul de fielduri respectiv clasei cu care este apelata metoda
	 */
	public Field[] createTableHeader() {
		Field[] fields=classType.getDeclaredFields();
		return fields;
	}
	
	/**
	 * @param fields vectorul de fielduri pentru care sa vor gasi numele
	 * @return vector de stringuri ce reprezinta numele atributelor clasei
	 */
	public ArrayList<String> tableHeader(Field[] fields){
		ArrayList<String> strings=new ArrayList<>();
		for (Field field :fields ) {
			strings.add(field.getName());
		}
		return strings;
	}
	
	
	/**
	 * @return Un string ce reprezinta statementul pentru select *
	 */
	public String createFindAllQuery() {
		String query="SELECT * FROM "+classType.getSimpleName();
		return query;
	}
	
	/**
	 * @param id id-ul pentru care se face update
	 * @param s lista de stringuri reprezentand coloanele ce vor fi updatate
	 * @return query-ul specific unui update dupa id
	 */
	public String createUpdateByIdQuery(String id, ArrayList<String> s) {
		String query="UPDATE "+classType.getSimpleName()+" SET ";
		Field[] fields=createTableHeader();
		for(int i=0;i<fields.length;i++) {
			query=query+fields[i].getName()+"='"+s.get(i)+"',";
		}
		query=query.substring(0, query.length()-1);
		query+=" WHERE id="+id;
		return query;
	}
	
	/**
	 * @param id id-ul dupa care sa efectueaza stergerea
	 * @return query-ul specific stergerii dupa id
	 */
	public String createDeleteByIdQuery(String id) {
		String query="DELETE FROM "+classType.getSimpleName()+" WHERE id="+id;
		return query;
	}
	
	/**
	 * @param s lista cu numele atributelor clasei
	 * @return query-ul specific introduceri unei linii
	 */
	public String createInsertQuery(ArrayList<String> s) {
		String query="INSERT INTO "+classType.getSimpleName()+" VALUES(";
		for(int i=0;i<s.size();i++)
			query+="'"+s.get(i)+"',";
		query=query.substring(0, query.length()-1);
		query+=")";
		return query;
	}
	
	
	/**
	 * @param quantity cantitatea la care se updateaza
	 * @param id id-ul dupa care s e face update
	 * @return query-ul specific updatari unei cantitati dupa id
	 */
	public String createUpdateQuantityQuery(int quantity, int id) {
		String query="UPDATE "+classType.getSimpleName()+" SET quantity=quantity-"+quantity+" WHERE id="+id;
		return query;
		
	}
	
	/**
	 * @param id id-ul dupa care se cauta inregistrarea
	 * @return query-ul specific cautarii dupa un id
	 */
	public String createFindById(int id) {
		String query="SELECT * FROM "+classType.getSimpleName()+" WHERE id="+id;
		return query;
	}
	
	/**
	 * metoda executa un query de tip update
	 * @param query query de tip insert/update/delete
	 */
	public void execUpdate(String query) {
		System.out.println(query);
		PreparedStatement preparedStatement=null;
		try {
	        preparedStatement=connection.prepareStatement(query);
			preparedStatement.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.Close(preparedStatement);
		}
	
	}
	
	/**
	 * @param fields vector de fielduri al clasei apelante
	 * @return valorile atributelor clasei apelante
	 */
	public ArrayList<String> getValues(Field[] fields) {
		ArrayList<String> strings=new ArrayList<>();
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				Object value=field.get(this);
				strings.add(value+"");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return strings;
	}	
	
	/**
	 * @param fields vector de fielduri ql clasei apelante
	 * @return valoeara campului id din clasa apelanta
	 */
	public String getIdValue(Field[] fields) {
		String valueOfId="";
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				if(field.getName().equals("id")) {
					Object value=field.get(this);
					valueOfId=value+"";
					
				}
			} catch (Exception e) {}
		}
		return valueOfId;
	}
	
	/**
	 * executa o operate de inserare in baza de date
	 */
	public void insert() {
		execUpdate(createInsertQuery(getValues(createTableHeader())));
	}
	
	/**
	 * executa o operate de update asupra bazei de date
	 */
	public void update() {
		execUpdate(createUpdateByIdQuery(getIdValue(createTableHeader()), getValues(createTableHeader())));
	}
	
	/**
	 * executa o operatie de delete asupra bazei de date
	 */
	public void delete() {
		execUpdate(createDeleteByIdQuery(getIdValue(createTableHeader())));	
	}
	
	/**
	 * @param query query-ul ce va fi executat
	 * @return tip de date ce stocheaza inregistrarile bazei de date
	 */
	public ResultSet execQuery(String query) {
		System.out.println(query);
		ResultSet resultSet=null;
		PreparedStatement preparedStatement=null;
		try {
			preparedStatement=connection.prepareStatement(query);
			resultSet=preparedStatement.executeQuery();
		} catch (Exception e) {}

		return resultSet;
	}

	/**
	 * @param resultSet tip de data ce stcocheaza inregistrari ale bazei de date
	 * @return lista de obiecte instantiate ale clasei apelante
	 */
	public ArrayList<T> findAllData(ResultSet resultSet){
		ArrayList<T> list=new ArrayList<>();
		try {
			while(resultSet.next()) {
				T instance=classType.newInstance();
				for (Field field : createTableHeader()) {
					Object value=resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor=new PropertyDescriptor(field.getName(), classType);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	

	

}
