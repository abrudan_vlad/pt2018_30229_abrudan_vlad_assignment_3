package Model;

import DataAccess.GenericAccess;

public class Orders extends GenericAccess<Orders> {
	private int id;
	private int productId;
	private int customerId;
    private String date;
    private String customerName;
    private String productName;
    private int quantity;
    
    
    public Orders(int id,int customerId,int productId,String date,String customerName,String productName,int quantity) {
    	this.id=id;
    	this.customerId=customerId;
    	this.productId=productId;
    	this.date=date;
    	this.productName=productName;
    	this.customerName=customerName;
    	this.quantity=quantity;
    }
    public Orders() {
    	this.id=0;
    	this.customerId=0;
    	this.productId=0;
    	this.date="";
    	this.customerName="";
    	this.productName="";
    }
    
    
    /**
     * metoda are posibilitatea de a prelua id-ul clientului
     * @return id-ul clientului
     */
    public int getCustomerId() {
		return customerId;
	}
    /**
     * metoda are posibilitatea de a prelua numele clientului
     * @return numele clientului
     */
    public String getCustomerName() {
		return customerName;
	}
    /**
     * metoda are posibilitatea de a prelua data in care a fost facuta comanda 
     * @return date
     */
    public String getDate() {
		return date;
	}
    
    /**
     * metoda are posibilitatea de a prelua id-ul de la comanda
     * @return id-ul comenzii
     */
    public int getId() {
		return id;
	}
    /**
     * metoda are posibilitatea de a prelua id-ul produsului
     * @return id-ul produsului
     */
    public int getProductId() {
		return productId;
	}
    /**
     * metoda are posibilitatea de a prelua numele produsului
     * @return numele produsului
     */
    public String getProductName() {
		return productName;
	}
    
    /**
     * metoda atribuie atributului "customerId" o valoare
     * @param customerId id-ul clientului
     */
    public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
    /**
     * metoda atribuie atributului "customerName" o valoare
     * @param customerName numele clientului
     */
    public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
    /**
     * metoda atribuie atributului "date" o valoare
     * @param date data in care  fost facuta comanda
    */
    public void setDate(String date) {
		this.date = date;
	}
    /**
     * metoda atribuie atributului "id" o valoare
     * @param id id-ul comenzii
     */
    public void setId(int id) {
		this.id = id;
	}
    /**
     * metoda atribuie atributului "productId" o valoare
     * @param productId id-ul produsului
     */
    public void setProductId(int productId) {
		this.productId = productId;
	}
    /**
     * metoda atribuie atributului "productName" o valoare
     * @param productName numele produsului
     */
    public void setProductName(String productName) {
		this.productName = productName;
	}
    /**
     * metoda atribuie atributului "quantity" o valoare
     * @param quantity cantitatea comenzii
     */
    public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
    /**
     * metoda preia o valoare
     * @return cantitatea comenzii
     */
    public int getQuantity() {
    	return quantity;
    }

    public String toString() {
    	return id+","+customerId+","+productId;
    }

}
