package View;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class TableView extends JFrame{
	JTable table;
	public TableView(JTable table) {
		this.table=table;
		JScrollPane scrollPane=new JScrollPane(table);
		this.setContentPane(scrollPane);
		this.setVisible(true);
		this.setLocation(200, 10);
		this.setTitle("Table pane");
		this.setSize(500, 500);
		
	}

}
