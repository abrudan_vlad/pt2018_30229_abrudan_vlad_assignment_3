package Model;

import DataAccess.GenericAccess;

public class Provider extends GenericAccess<Provider> {
	private int id;
	private int productId;
	private String productName;
	private int productQuantity;
	
	public Provider() {
		id=0;
		productId=0;
		productName="";
		productQuantity=0;
	}
	
	public Provider(int productId) {
		this.productId=productId;
	}
	
	public Provider(int id, int productId, String productName,int productQuantity) {
		this.id=id;
		this.productId=productId;
		this.productName=productName;
		this.productQuantity=productQuantity;
		
	}

	/**
	 * @return returneaza valoarea atributului id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @return returneaza valoarea atributului id
	 */
	public int getProductId() {
		return productId;
	}
	
	/**
	 * @return returneaza valoarea atributului id
	 */
	public String getProductName() {
		return productName;
	}
	
	/**
	 * @return  returneaza valoarea atributului id
	 */
	public int getProductQuantity() {
		return productQuantity;
	}
	
	/**
	 * seteaza valoarea atributului id
	 * @param id valoarea atributului id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * seteaza valoarea campului productId
	 * @param productId valoarea atributlui productId
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	/**
	 * seteaza valoarea atributului productName
	 * @param productName valoarea atributului productName
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/** seteaza valoarea atributlui productQuantity
	 * @param productQuantity valoarea atributlui productQuantity
	 */
	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
	
}
