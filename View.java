package View;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class View extends JFrame{
 private JButton admin=new JButton("Admin Panel");
 private JButton customer=new JButton("Customer Panel");

 public View() {
	 JPanel panel=new JPanel();
	 panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	 panel.add(customer);
	 panel.add(admin);
	 this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 this.setSize(new Dimension(300,180));
	 this.setLocation(400, 200);
	 this.setContentPane(panel);
	 this.setVisible(true);
	 this.setTitle("Main menu");
 }
 
/**
 * @return the button for admin opption
 */
public JButton getAdmin() {
	return admin;
}

/**
 * @return the button for customer opption
 */
public JButton getCustomer() {
	return customer;
}
}
