package DataAccess;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ConnectionFactory {

	private static final Logger LOGGER=Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DRIVER="com.mysql.jdbc.Driver";
	private static final String DBURL="jdbc:mysql://localhost:3306/schooldb";
	private static final String USER="root";
	private static final String PASS="magictrick1";
	private static ConnectionFactory singleInstance=new ConnectionFactory();
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();		}
	
	}
	
	/**
	 * creaza unica instanta a clasei
	 * @return unica conexiune la baza de date
	 */
	private Connection createConnection() {
		 Connection connection=null;
		try {
			 connection=DriverManager.getConnection(DBURL,USER,PASS);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();		}
		return connection;
	}
	
    /**
     * @return unica conexiune la baza de date
     */
    public  static Connection getConnection() {
		return singleInstance.createConnection();
	}
    
    /**
     * @param connection conectiunea ce urmeaza sa fie inchisa
     */
    public static void close(Connection connection) {
    	if(connection!=null)
    	try {
			connection.close();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();		}
    }
    
    /**
     * @param statement statemenul ce urmeaza sa fie inchis
     */
    public static void Close(Statement statement) {
    	if(statement!=null)
    		try {
    			statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    }
	
    /**
     * @param resultSet resultSet-ul ce urmeaza sa fie inchis
     */
    public static void close(ResultSet resultSet) {
    	if(resultSet!=null)
    	try {
			resultSet.close();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "An error occured while trying to connect to the database");
			e.printStackTrace();		}
    }
    
    
}
