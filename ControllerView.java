package BussinesLogic;
import java.awt.event.ActionEvent;

import View.View;
import java.awt.event.ActionListener;


public class ControllerView {
	
	private View view=new View();
	
	public ControllerView() {
		view.getAdmin().addActionListener(new ListennerAdmin());
		view.getCustomer().addActionListener(new  ListennerCustomers());
		
		
	}
	
	class ListennerCustomers implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			ControllerCustomer controllerCustomer=new ControllerCustomer();
		}
	}
	
	class ListennerAdmin implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unused")
			ControllerAdmin controllerAdmin=new ControllerAdmin();
		}
	}


}
